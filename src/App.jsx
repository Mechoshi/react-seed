import React, { Component, Fragment, lazy, Suspense } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import styles from './App.scss';
import { LazyNotFound, LazyPlayground } from './pages';
import { Loader, Navigation } from './components';

export class App extends Component {
  state = {
    message: 'Hi'
  };

  render() {
    const { message } = this.state;

    return (
      <main className={styles.App}>
        <BrowserRouter>
          <>
            <Route path="/" component={Navigation} />
            <Suspense fallback={<Loader />}>
              <Switch className={styles.App}>
                <Redirect exact={true} from="/" to="/playground" />
                <Route exact={true} path="/playground" component={LazyPlayground} />
                <Route path="*" component={LazyNotFound} />
              </Switch>
            </Suspense>
          </>
        </BrowserRouter>
      </main>
    );
  }
}

export default App;
