import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './Navigation.scss';

export const Navigation = props => {
  return (
    <nav className={styles.Navigation}>
      <NavLink className={styles.NavLink} activeClassName={styles.Active} to="/playground">
        Playground
      </NavLink>
      <NavLink className={styles.NavLink} activeClassName={styles.Active} to="/notfound/page">
        404
      </NavLink>
    </nav>
  );
};

export default Navigation;
