import React from 'react';

import styles from './NotFound.scss';

export const NotFound = props => {
  return (
    <div className={styles.NotFound}>
      <h1>Not Found</h1>
    </div>
  );
};

export default NotFound;
