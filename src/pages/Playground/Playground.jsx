import React, { Component } from 'react';

import styles from './Playground.scss';

export class Playground extends Component {
  render() {
    return (
      <div className={styles.Playground}>
        <h1>This is Playground</h1>
      </div>
    );
  }
}

export default Playground;
