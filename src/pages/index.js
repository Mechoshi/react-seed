import React, { lazy } from "react";

const NotFound = lazy(() => import('./NotFound/NotFound.jsx'));
export const LazyNotFound = props => <NotFound {...props}/>;

const Playground = lazy(() => import('./Playground/Playground.jsx'));
export const LazyPlayground = props => <Playground {...props}/>;
