import axios from 'axios';

export const ajax = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/'
});

ajax.interceptors.request.use(
  res => {
    console.log('request', res);
    return res;
  },
  error => {
    console.error('request', error);
    return Promise.reject(error);
  }
);

ajax.interceptors.response.use(
  res => {
    console.log('response', res);
    return res;
  },
  error => {
    console.error('response', error);
    return Promise.reject(error);
  }
);
