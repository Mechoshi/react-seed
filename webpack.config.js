const autoprefixer = require('autoprefixer');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = (env, argv) => {
  const isDev = argv.mode === 'development';
  const config = {
    entry: {
      app: './src/index.js'
    },
    output: {
      filename: isDev ? 'public/[name].js' : 'public/[name].[chunkhash].js',
      chunkFilename: isDev ? 'public/[name].chunk.js' : 'public/[name].chunk.[chunkhash].js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },
    resolve: {
      extensions: ['.js', '.jsx', '.scss']
    },
    module: {
      rules: [
        {
          test: /\.worker\./,
          use: { loader: 'worker-loader' }
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.(woff|woff2|ttf|eot)$/,
          exclude: /node_modules/,
          loader: 'file-loader?name=public/fonts/[name].[ext]'
        },
        {
          test: /\.(png|jpe?g|gif|svg|ico)$/,
          exclude: /node_modules/,
          loader: 'file-loader?name=public/images/[name].[ext]'
        },
        {
          test: /\.scss$/,
          use: [
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                importLoaders: 3,
                modules: {
                  localIdentName: '[name]__[local]__[hash:base64:5]'
                },
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [autoprefixer()]
              }
            },
            {
              loader: 'sass-loader'
            },
            {
              loader: 'sass-resources-loader',
              options: {
                resources: require(path.join(__dirname, 'src', 'styles', 'shared', 'index.js'))
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: 'dist'
      }),
      new HtmlWebPackPlugin({
        template: './src/index.html'
      }),
      new webpack.DefinePlugin({
        __mode__: JSON.stringify(argv.mode)
      })
    ],
    optimization: {
      runtimeChunk: {
        name: 'manifest'
      },
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /node_modules/,
            name: 'vendor',
            chunks: 'all'
          }
        }
      },
      minimizer: []
    },
    devtool: isDev ? 'cheap-module-eval-source-map' : 'source-map',
    devServer: {
      stats: 'minimal',
      historyApiFallback: {
        index: 'http://localhost:8080'
      }
    }
  };

  if (!isDev) {
    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: 'public/[name].[chunkhash].css',
        chunkFilename: 'public/[id].[chunkhash].css'
      })
    );
    config.plugins.push(new CompressionPlugin());
    config.optimization.minimizer.push(...[new UglifyJsPlugin(), new OptimizeCSSAssetsPlugin()]);
  } else {
    config.plugins.push(new BundleAnalyzerPlugin());
  }

  return config;
};
